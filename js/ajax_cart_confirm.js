/**
 * Created by Linas on 12/1/15
 */
(function ($) {
  Drupal.behaviors.ajax_cart_confirm = {
    closeDialog: function(unique) {
      $("#dialog-confirm").dialog('close');
    },

    confirmAddToCart: function(unique) {
      if($('#edit-submit--'+unique).hasClass('ajax-processed')) {
        $('#edit-submit--'+unique).trigger('mousedown');
      } else {
        $('.form-ajax-confirm-processed-'+unique).submit();
        $("#dialog-confirm").dialog('close');
      }
    }

  };
})(jQuery, Drupal);

<?php
/**
 * @file
 * Ajax form confirm admin pages.
 */

/**
 * Ajax form confirmation admin settings form.
 */
function ajax_form_confirm_admin_settings() {
  $form = array();
  $form['ajax_form_confirm_allowed'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed form ids'),
    '#default_value' => variable_get('ajax_form_confirm_allowed', 'ajax_form_confirm_admin_settings'),
    '#size' => 50,
    '#description' => t("Allowed form ids separated by comma"),
  );

  $form['ajax_form_confirm_denied'] = array(
    '#type' => 'textarea',
    '#title' => t('Disllowed form ids'),
    '#default_value' => variable_get('ajax_form_confirm_denied', ''),
    '#size' => 50,
    '#description' => t("Allowed form ids separated by comma"),
  );

  $form['ajax_form_confirm_message'] = array(
    '#type' => 'textarea',
    '#title' => t('Warning message'),
    '#default_value' => variable_get('ajax_form_confirm_message', ''),
    '#size' => 50,
    '#description' => t("The message to be displayed"),
  );

  $form['ajax_form_confirm_warning'] = array(
    '#type' => 'checkbox',
    '#default_value' =>  variable_get('ajax_form_confirm_warning', ''),
    '#title' => t('Show warning icon in the dialog window'),
    '#weight' => 46,
  );

  return system_settings_form($form);
}
